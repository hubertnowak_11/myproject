<?php
	session_start();
	if(!isset($_SESSION['user']))
	{
		header('Location:/logowanie/index.php');
		exit();
	}
 ?>
	<!DOCTYPE HTML>
<html lang="pl">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta charset="utf-8" />
	<title>Programing</title>

	<meta name="description" content="Programuj z nami!" />
	<meta name="keywords" content="Programming, c++, html, javaScript, PHP, C++ Object, MySQL, courses" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<link rel="stylesheet" href="style.css" type="text/css"/>

	<title>Font Awesome Icons</title>
	<meta name='viewport' content='width=device-width, initial-scale=1'/>
	<script src='https://kit.fontawesome.com/a076d05399.js'></script>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
	<link href="https://fonts.googleapis.com/css?family=Audiowide&display=swap" rel="stylesheet"/>

<script>

		function getCookie(cname)
		 {
				var name = cname + "=";
				var decodedCookie = decodeURIComponent(document.cookie);
				var ca = decodedCookie.split(';');
				for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
				}
				return "";
			}

        function zegarek()
        {
            var data = new Date();
            var godzina = data.getHours();
            var minuta = data.getMinutes();
            var sekunda = data.getSeconds();
            var dzien = data.getDate();
            var dzienN = data.getDay();
            var miesiac = data.getMonth();
            var rok = data.getFullYear();

            if (minuta < 10) minuta = "0" + minuta;
            if (sekunda < 10) sekunda = "0" + sekunda;
						if (godzina == 0) godzina = "0" + godzina;

            var dni = new Array("Sunday", "Monday", "Tuesday",
							"Wednesday", "Thursday", "Friday", "Saturday");
            var miesiace = new Array("January", "February", "March",
							"April", "May", "June", "July", "August",
							"September", "October", "November", "December");

            var pokazDate = "Today is " + dni[dzienN] + ', ' + dzien + ' '
							+ miesiace[miesiac] + ' ' + rok + "<br />Hour "
							+ godzina + ':' + minuta;

            document.getElementById("zegar").innerHTML = pokazDate;

            setTimeout(zegarek,1000);
        }

			function myFunction()

			{
				document.getElementById("kontakt").innerHTML = "Address e-mail:</br>hubert_nowak11@interia.pl</p>";
			}

			//function clearHello()
			//{
				//document.getElementById("hello").style.display = "none";
			//}
				//setTimeout("clearHello()", 8000);

			function checkCookieAccept()
		 	{
				var acceptCookie=getCookie("simplecookienotification_v01");
				if (acceptCookie!= "")
				{
				  document.getElementById("simplecookienotification_v01").style.display="none";
				}
			}
</script>

<script type="text/javascript">

	var galTable= new Array();
	var galx = 0;

</script>

<script type="text/javascript">

	function simplecookienotification_v01_create_cookie(name,value,days)
	{
		if (days)
		{
			var date = new Date(); date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
			else var expires = ""; document.cookie = name+"="+value+expires+"; path=/";
	 		document.getElementById("simplecookienotification_v01").style.display = "none";
	  }

	 function simplecookienotification_v01_read_cookie(name)
	 {
		 var nameEQ = name + "=";
		 var ca = document.cookie.split(";");
		 for(var i=0;i < ca.length;i++)
		 {
			 var c = ca[i];
			 while (c.charAt(0)==" ") c = c.substring(1,c.length);
			 if (c.indexOf(nameEQ) == 0)
			 return c.substring(nameEQ.length,c.length);
	   }
		 		return null;
	 }

	 	var simplecookienotification_v01_jest = simplecookienotification_v01_read_cookie("simplecookienotification_v01");

		if(simplecookienotification_v01_jest==1)
			{
				document.getElementById("simplecookienotification_v01").style.display = "none";
			}

		function game()
			{
				document.location.href = "/logowanie/HomePage/game/index.php";
			}

</script>
</head>

<body onload="zegarek(), checkCookieAccept()">

<div class="container" style="margin-left: auto; margin-right: auto;">

		<div id = "zegar" style="color: lightblue; text-shadow:3px 3px 10px aqua; font-size: 20px; text-align: right;"></div></br>

		<div id = "hello" style = "font-size: 40px; text-shadow: 3px 3px 15px aqua;">

			<?php

				//echo('<pre>'); var_dump($_SESSION);
				if(isset($_SESSION['user'])){
						echo '<p>'.'Hello '.$_SESSION['user']['user'].'</pl>';
					}

			 ?>

		</div>

		<h1>Choose a Course For Yourself</h1>

		<div class = "menu">

			<a href="https://miroslawzelent.pl/kurs-c++/"  target="_blank" title="Look into the world C++">
				<img src="picture/haker.jpg" style="border-radius: 25px; text-align:center;"/></a>

			<a href="https://miroslawzelent.pl/kurs-obiektowy-c++/" target="_blank" title="Look into the world C++ Object">
				<img src="picture/haker3.jpeg"  style="border-radius: 25px; text-align: right;"/></a>

			<a href="https://miroslawzelent.pl/kurs-html/" target="_blank" title="Look into the world HTML">
				<img src="picture/haker2.jpg"  style="border-radius: 25px; text-align: right;"/></a>

			<a href="https://miroslawzelent.pl/kurs-css/" target="_blank" title="Look into the world CSS">
				<img src="picture/haker3.jpg"  style="border-radius: 25px; text-align: right;"/></a>

		</div>

		<br></br>

		<div class = "menu">

			<a href = "https://miroslawzelent.pl/kurs-javascript/" target = "_blank" title = "Look into the world JavaScript">
				<img src = "picture/haker4.jpg"  style="border-radius: 25px;"/></a>

			<a href = "https://miroslawzelent.pl/kurs-php/" target = "_blank" title = "Look into the world PHP">
				<img src = "picture/haker6.jpg"  style="border-radius: 25px;"/></a>

			<a href = "https://miroslawzelent.pl/kurs-mysql/" target = "_blank" title = "Look into the world MySQL">
				<img src = "picture/haker7.jpg"/></a>

			<a href = "https://miroslawzelent.pl/kurs-bootstrap/" target = "_blank" title = "Look into the world Bootstrap">
				<img src = "picture/bootstrap.jpg"/></a>

		</div>

		<br></br>
		<br></br>
		<br></br>

	<div>

		<div class = "opcje">

			<a href="https://rogerdudler.github.io/git-guide/index.pl.html" target="_blank" title="Check git">
				<img class='opcje' src="picture/git.jpg"  style="border-radius: 25px; text-align: right;"/></a>
			<a href='https://www.facebook.com/hubert.nowak.71697/' target='_blank' title='Meet me'>
				<img class='opcje' src="picture/facebook.jpg"  style="border-radius: 25px; text-align: right;"/></a>
			<a href='https://stackoverflow.com/' target='_blank' title='Check StackOverflow'>
				<img class='opcje' src="picture/stackOverflow.jpg"  style="border-radius: 25px; text-align: right;"/></a>

		</div>

   </div>

		<br></br>

		<div class="button" style=" text-align: center;">
			<button onclick="game()" style="border-radius:10px; cursor:pointer;
			text-shadow: 2px 2px 15px gray; background-color: black; font-size: 25px;
			text-align: center; ">GAME</button>
			</p>
		</div>

		<div class="button" id = 'kontakt' style=" text-align: center;">
			<button onclick="myFunction()"
			style="border-radius:10px; cursor:pointer; text-shadow: 2px 2px 15px gray;
			background-color: black; font-size: 25px; text-align: center; ">Contact</button>
			</p>
		</div>

		<div>
			<a class="button" href="logout.php">Log out</a>
		</div>

		<div id="simplecookienotification_v01" style="display: block; z-index: 99999;
			min-height: 35px; width: 100%; position: fixed; background: rgb(31, 31, 31);
			border: 0px rgb(198, 198, 198); text-align: center; right: 0px;
			color: rgb(119, 119, 119); bottom: 0px; left: 0px; border-radius: 0px;">

			<div style="padding:5px; margin-left:15px; margin-right:15px; font-size:17px; font-weight:normal;">
				<span id="simplecookienotification_v01_powiadomienie" style="margin-right:25px;">This site uses cookies</span>
					<span id="br_pc_title_html"><br></span>

				<a id="simplecookienotification_v01_polityka" href="http://jakwylaczyccookie.pl/polityka-cookie/"
					target="_blank" style="font-size: 14px; color: rgb(198, 198, 198);">Privacy Policy</a>
					<span id="br_pc2_title_html"> &nbsp;&nbsp; </span>

				<a id="simplecookienotification_v01_info" href="http://jakwylaczyccookie.pl/jak-wylaczyc-pliki-cookies/"
				 target="_blank" style="font-size: 14px; color: rgb(198, 198, 198);">How to turn off cookies</a>
					<span id="br_pc3_title_html"> &nbsp;&nbsp; </span>

				<a id="simplecookienotification_v01_info2" href="https://nety.pl/cyberbezpieczenstwo"
					target="_blank" style="font-size: 14px; color: rgb(198, 198, 198);">Cybersecurity</a>
					<div id="jwc_hr1" style="height: 10px; display: none;"></div>

				<a id="okbutton" href="javascript:simplecookienotification_v01_create_cookie('simplecookienotification_v01',1,7);"
					style="position: absolute; background: red; color: rgb(255, 255, 255);
					padding: 5px 15px; text-decoration: none; font-size: 7px;
					font-weight: normal; border: 0px solid rgb(31, 31, 31);
					border-radius: 5px; top: 5px; right: 1px;">X</a>

					<div id="jwc_hr2" style="height: 10px; display: none;"></div>
			</div>
		</div>

			<h2>&copy;2019. Hubert Nowak. All rights reserved.</h2>

</div>

</body>
